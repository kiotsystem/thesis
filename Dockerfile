FROM arnulfosolis/latex:alpine

ARG DOCUMENT_NAME

WORKDIR /document

# copy our makefile
COPY ./Makefile ./Makefile

# copy our binaries
COPY ./bin/ ./bin/

# set Make as our entrypoint
ENTRYPOINT ["/usr/bin/make"]

CMD ["pdf"]
