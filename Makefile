# Load .env file if existing
-include .env

pdf:
	. ./bin/render_pdf.sh ${MAIN_FILENAME}
.PHONY: pdf

build:
	#docker-compose run latex
	docker-compose up --abort-on-container-exit --exit-code-from latex
.PHONY: build

html:
	htlatex ${MAIN_TEX_FILENAME}
.PHONY: html

clean:
	find . -name "*.log" -type f -delete
	find . -name "*.aux" -type f -delete
	find . -name "*.tmp" -type f -delete
	find . -name "*.tex.bak" -type f -delete
	find . -name "*.4ct" -type f -delete
	find . -name "*.4tc" -type f -delete
	find . -name "*.xref" -type f -delete
	find . -name "*.out" -type f -delete
	find . -name "*.toc" -type f -delete
	find . -name "*.bbl" -type f -delete
	find . -name "*.blg" -type f -delete
	find . -name "*.acn" -type f -delete
	find . -name "*.bbl" -type f -delete
	find . -name "*.glo" -type f -delete
	find . -name "*.glsdefs" -type f -delete
	find . -name "*.ist" -type f -delete
	find . -name "*.log" -type f -delete
	find . -name "*.lol" -type f -delete
	find . -name "*.lof" -type f -delete
	find . -name "*.out" -type f -delete
	find . -name "*.toc" -type f -delete
	docker-compose down
.PHONY: clean
