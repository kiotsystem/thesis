\chapter{Implementation}

In this chapter the implementation details of the KIoT System are described.
Although the KIoT System can be modeled in multiple ways, this thesis proposes a way of isolating runtime configurations and to increase portability through the use of Docker containers.
In this chapter the implementation details for the Automation tools and the IoT platform are defined and discussed.

\section{Automation Tools}

The KIoT System embraces continuous practices, empowering developers to deliver artifacts more frequently.
One of the subsystems from this component is Version Control System.
A Version Control System keeps track of all the different versions from a set of files.
Implementations of this system already exist and are vast, e.g., Git, mercurial and Subversion (SVN). In this thesis, we work closely with Git, allowing us to have flexible branching models and take advantage of its distributed nature.
Git branches allow to diverge from the main line of development and work against it without altering any other.
Unlike other VCS implementations Git enourages to branch out and merge often.
While the code base is centralized in a Git provider, in essence, Git allows developers to have an exact copy of the system locally and work against it.
Once the work is done, the local state of the branch is pushed to the remote repository's branch, hence the nature of the distributed VCS.
These enables developers to work concurrently against a centralized code base.

One of the most common branch models is the Release Candidate.
This model splits the repository into development line and the production line.
While the development line can be unstable, the production line (\emph{branch}) is always considered to be stable and, most of the times, merging to this branch is done from a release candidate branch and by a Quality Assurance.
Release candidate branches allows to test a set of features and fix them accordingly to the tests results.
Once the branch passes the quality requirements the branch is then merged to the production branch and back to the development branch.
When a commit in the production branch or a release tag is done an automatic deployment is triggered.

The next big component of the Automation tools system is the \acrshort{CI} server.
\acrshort{CI} Servers overall enable the development cycle to have sequences of predefined steps that are executed in every \emph{branch commit} (version) of the tracked artifact (repository).
This enables the artifacts to have a uniform build process, reproducible automated tests, and reliable feedback.
Implementations of CI servers are vast and every implementation aims to address the problem in different ways.
Common implementations are Jenkins \cite{ref:jenkins}, travis, concourse, and Drone.io \cite{reference:drone_ci}.
CI servers have nowadays evolved around cloud applications and encounter problems with cross architecture compatibilities.
Therefore, when dealing with ARM architecture hosts, the few options available are Jenkins and Drone.io.

Drone.io is a relative young Open-Source project that has gained special attention among Docker based workloads.
This is due to the special approach that Drone took, \emph{everything is a container}.
The Drone server, that being the master, is deployed as a single Docker container and handles all administrative logic and scheduling of workloads.
The Drone agents, are as well deployed as a single container each, and carry out the assigned workload given by the master.
This model allows the server to be distributable among a set of hosts independently of their architecture and enables artifact pipelines to be executable in their respective architecture while taking advantage of the portability of the container technology, resulting in low server maintenance.

\begin{figure}[h]
  \includegraphics[width=0.75\textwidth]{content/images/04_implementation/automation-tools.pdf}
  \centering
  \caption{Automation tools for the KIoT System.}
  \label{diagram:kiot-automation-tools}
\end{figure}

As illustrated in the figure \ref{diagram:kiot-automation-tools}, the KIoT's Automation tools is composed by a Git provider and a distributed CI Server.
In this thesis, however, the automation tools are deployed in different cloud providers, allowing the use of a Git service provider, e.g., Bitbucket Cloud.

When a state change occurs in the VCS, the system invokes a build for the given commit by sending a hook, e.g., an HTTP Post request to the CI master.
The Drone master will then read the configuration file and schedule the stages through out the workers and execute the commands inside the specified container image.
The steps can be executed sequentially or parallel, however, once one of the steps exits with a non-zero code, the whole pipeline is marked as fail and the following steps are canceled and the pipeline results can be consulted through the master's UI or API endpoint.

Building docker containers from within a container is possible by mounting the docker's UNIX Socket into the container, the docker CLI in the container will communicate with the host's docker engine.
This can be manually done, however, Drone offers a plugin for automating the process of building and publishing to a container image registry, e.g., Docker's registry.

As it is illustrated in listing \ref{code:drone}, a Python application targeted to the \emph{arm} architecture is to be tested, built, and deployed.
 
\lstinputlisting[language=yaml, caption={Example of a drone pipeline configuration}, label=code:drone]{content/code/automation-tools/drone.yml}

\section{IoT Platform}

In order for the KIoT system to handle network partitions and tolerate node failures, the applications have to be managed by an entity, network has to be uniform and transparent to all nodes.

Thus, in the KIoT platform is imperative to follow the UNIX process model for running service daemons. This means that by considering processes as a first class citizen, applications are achitected into different process types. Therefore, in order to follow the process model we require a process manager, hence Kubernetes.

\begin{figure}[h]
  \includegraphics[width=0.52\textwidth]{content/images/04_implementation/kiot.pdf}
  \centering
  \caption{IoT Platform.}
  \label{diagram:kiot-implementation}
\end{figure}

As it is illustrated in figure \ref{diagram:kiot-implementation}, the KIoT system is composed by distributed nodes.
A requirement of IoT platforms is to support IoT Gateway applications, these applications typically depend of block devices for communicating with external hardware.
By mounting the block device to the gateway container, the process will have access to use the block device.
Therefore, there are two deployment strategies for gateway applications.
The first, is to deploy the Gateway service in all the nodes that have the block devices mounted.
In contrast, the second is to deploy the service in specific nodes.
The latter is usefull when dealing with different network protocols and resources are not to be wasted by deploying unnecessary services.
Both solutions can be achieved through the \emph{DaemonSet} or \emph{Deployments}, and \emph{label or node selectors}.
Using \emph{DaemonSet} will guarantee that the specified Pod will run in all matched nodes, in contrast \emph{Deployments} objects will only allocate the replicas described in the specification.
In K8s, \emph{Deployments} are best fitted for stateless applications, e.g., \emph{Digital Twins}.

\emph{Digital Twins} in the KIoT system are to be architected in a stateless nature due to the volatility of the IoT platform and the abstraction from resources.
Any state to be persisted must go to a stateful service, e.g., in-memory datastores (redis or memcached) or databases hosted in devices that are meant to be highly available.
Therefore, the nature of an event driven platform makes it more portable across a set of volatile nodes while preserving their stateless nature.

\begin{figure}[h]
  \includegraphics[width=0.5\textwidth]{content/images/04_implementation/network.pdf}
  \centering
  \caption{Network for the KIoT platform.}
  \label{diagram:kiot-network}
\end{figure}

\newacronym{IPVS}{IPVS}{IP Virtual Server}

As the KIoT uses the Internet as a backbone network, in some cases a certain privacy and security are required.
For such cases, the IoT platform can be attached to an IP provided by a VPN server.
This will expose the IoT platform only to members of the network and allow encrypted traffic over an insecure medium, this is illustrated in figure \ref{diagram:kiot-network}.
Different VPN implementations are available, however, in this thesis the implementation occurred through a self-hosted OpenVPN for best flexibility, e.g., clients must have static IPs and tunneling all the traffic over the tunnel interface is not required.
The usage of a VPN also allows nodes to overcome problems commonly found in subnetworks where some sort of NAT sits in front.
This implementation, however, requires the \emph{kube-proxy} to use \acrfull{IPVS} (default in K8s 1.11+) instead of the previous default \emph{iptables} due to configuration interference of the OpenVPN service.
This allows the K8s CNI implementation, in this case Weave Net, to work transparently across all devices.

Device management is a quite unique issue in the IoT, this is because device management of up to hundreds of devices is complex.
The KIoT system only allows node management through the centralized Kubernetes dashboard.
For visualization and monitoring of nodes, the KIoT System recomends implementing Weave Scope.
Weave Scope allows real-time monitoring of nodes and containers, while enabling remote shell interaction with them.
\emph{Weave Scope} creates a \emph{DaemonSet} for local metrics collection, while allocating a master agent in the master node that serves all the metrics through a UI.

For device management over Gateway applications, these applications can be implemented by communicating over the MQTT broker. In this case, management applications will contact the devices over a topic and the device should react accordingly to the messages translated over the Gateway application. Implementations of a MQTT Broker are vast \cite{reference:mqtt_brokers_github}, in this thesis RabbitMQ with the MQTT and Web MQTT plugin activated is implemented. The benefit over other MQTT implementations is due to RabbitMQ's open-source nature and offical plugin support for the MQTT 3.1.1 protocol. RabbitMQ's nature makes it easy to deploy message brokers in distributed and federation configurations \ref{appendix:rabbitmq}.


\section{Digital Twin}

For IoT applications to benefit from Kubernetes' layer of abstraction is imperative the application layer into different processes following the UNIX process model. This guarantees more scoped applications and enable the \emph{scheduler} to predict resource consumption more accurately and schedule resources more efficiently throughout the cluster. Therefore, the application layer of an IoT application should be split into two processes, a translation process and a service process.

\subsection{From the lower process}

The lowest level process, namely the translation process, provides translation of higher level applications. This process translates the communication from the mounted block device to a higher level communication protocol. In essence, as illustrated in figure \ref{diagram:unix_socket}, the lower process mounts the block devices and uses it to communicate with the external device while exposing a UNIX Domain Socket which handles higher language requests that represent the LED state. In this example, a UNIX Domain Socket was exposed, however, is possible to expose a UDP or TCP based stream.

\begin{figure}[h]
  \centering
  \begin{minipage}{0.45\textwidth}
    \centering
    \includegraphics[width=0.95\textwidth]{content/images/04_implementation/unix_socket.pdf}
    \caption{Communication between highly coupled processes.}
    \label{diagram:unix_socket}
  \end{minipage}\hfill
  \begin{minipage}{0.45\textwidth}
    \centering
    \includegraphics[width=0.95\textwidth]{content/images/04_implementation/digital_twin.pdf}
    \caption{Communication between processes through MQTT.}
    \label{diagram:digital_twin}
  \end{minipage}\hfill
\end{figure}

\subsection{Coupled processes}

The upper process, the abstraction of the mounted device, is in other words the \emph{Digital Twin}. \emph{Digital Twins} talk only higher level protocols like MQTT, HTTP, or websockets as illustrated in figure \ref{diagram:digital_twin}. Therefore, this process can be reallocated on node failure and provide coherent responses by either responding \emph{Out of Service} responses or a cached value.

% implementation: is like a *tutorial*
% Full configuration goes into Appendix but interesting part into implementation
An example of a tightly coupled IoT application is an LED resource.
LED applications have the simple task of toggling the GPIO state.
By applying the process model, two processes are to be executed, a translator and a service.
The translator interacts directly with the GPIO pins of the board and setting the incoming state, as described in listing \ref{code:led-translator}.
On process boot, the application creates and binds to a local socket.
In order to share this socket between the processes, the directory the socket is mounted in, is defined as a volume and mounted all the defined containers \ref{code:ledPod}.

In this example, the socket endpoint is JSON encoded strings based and applies the incoming LED state through the \emph{led} module \ref{code:ledMod}. The communication protocol over the Unix Socket is established before-hand, in this case, the translator process parses the incoming message, applies the state to the specified GPIO pin, and echoes the value back.

Different communication models exist, for example, TCP or UDP. The benefit of UNIX sockets over other models is that the socket is mounted into the file system rather than attaching it through an IP and port, reducing the network overhead.
However, attaching to the \emph{localhost} will allow processes in the same pod to communicate over the same IP and will then share the same local network bridge.

% explain the concepts by code
\lstinputlisting[language=Rust, frame=single, numbers=left, caption=UnixStream handling in Rust, label=code:led-translator]{content/code/led/translator.rs}

As described in listing \ref{code:led-service}, the LED class abstracts the socket communication by exposing a \emph{bind} method. This method, binds an instance of the LED to a socket at the specified path. In which at every state change, the LED instance will trigger the communication with the translator process for it to apply the new state \ref{code:ledUds}.

When a state change ocurrs, the service process dumps the LED as a sequence of bytes and sends it to the specified MQTT topic. In this case, the communication protocol over the UNIX socket is implemented according to the protocol specified by the translator process, for more information refer to the code included in the appendix \ref{code:ledUds}.

% include abstracted functions led-uds and from service
\lstinputlisting[language=Python, caption=Service Toggling the LED, label=code:led-service]{content/code/led/service.py}

For this two processes to communicate between each other they require the socket in their respective file system. As each process is ran inside a container, a volume mount is required. In listing \ref{code:ledPod}, it is described how a volume mount can be defined in the Pod specification and how it is mounted in the container definitions. For the Pod to be controlled by a \emph{controller}, the pod definition has to be registered at the \emph{Deployment Controller}, therefore, creating a deployment object [\ref{code:ledDeployment}] that defines the desire Pod Specification will ease pod updates and enable tracking the deployments.
So when a deployment is done, the \emph{Deployment Controller} keeps track of the changes done through deployments. This allows to rollback deployments by specifying which deployment version should be the actual one.

When describing the deployment of a pod [\ref{code:ledDeployment}], processes that attach to Block Devices, e.g., \emph{/dev/gpiomem}, is recommended to use the \emph{Recreate} deployment strategy. This strategy will stop the actual one and start the new only after the actual is stopped. This will prevent conflicts in the communication with block devices.

When describing a pod, environment variables are used to configure application runtime. Environment variables are assigned to the executing process and, therefore, more portable than any other configuration method. These variables are available from the begining of the process and inherit to subprocesses. In this case, the container specification from the pod specification \ref{code:ledPod} defines all those values.

\lstinputlisting[language=yaml, caption=LED Pod specification, label=code:ledPod]{content/code/led/podSpec.yml}
\newpage

A pod and container network overview is illustrated in figure \ref{picture:pod-network} and \ref{picture:container-network}.
% explain how the Pod is built by 2 processes
\begin{figure}[h]
  \centering
  \begin{minipage}{0.5\textwidth}
    \centering
    \includegraphics[width=0.7\textwidth]{content/images/04_implementation/pod-network.png}
    \caption{Pod networking.}
    \label{picture:pod-network}
  \end{minipage}\hfill
  \begin{minipage}{0.45\textwidth}
    \centering
    \includegraphics[width=0.8\textwidth]{content/images/04_implementation/container-network.png}
    \caption{Container networking.}
    \label{picture:container-network}
  \end{minipage}\hfill
\end{figure}

\subsection{Distributed applications}

However, when IoT applications become more complex, such as IoT gateway applications, the model slightly evolves.
These applications then become the translation processes as illustrated in figure \ref{diagram:digital_twin}.
For higher level applications, \emph{Digital Twins} act as the abstraction process.
This enables to carry out computational tasks in more reliable nodes, use resources across the node cluster more efficiently, and to expose the resources to other cloud components in a more convenient format.

In this case, gateway applications become as stateless as possible.
Therefore, gateways translate protocols and carries out simple device management tasks, e.g., handle device reconnects, device registrations, or device redundancy aggregation.

\subsubsection{Things}

An example of an application at the 7th layer of the OSI Model is the Remote Sensors framework \cite{reference:remote-sensors-api}. This framework abstracts sensors or peripherals from constrained devices into resources and exposes them using URIs.
The framework targets constrained devices that use specialized network protocols, such as the ZigBee or IEEE 802.15.4.
The Remote Sensors protocol provides two communication methods, a \emph{request/response} and \emph{stream} based communication.
What's special about this protocol is the exposability of the resources.
On device boot, the resources exposed are sent to the gateway. This allows the gateway to index resources and manage them accordingly.
Moreover, the protocol allows on demand resource requests through the \emph{request/response} method.
However, for situations where data is monitored, data streams are desired.
For this the framework supports streaming of data at desired frequencies.

In this case, a temperature sensor is exposed as a resource. The Remote Sensors framework allow handlers to act as an entrypoint for the request, in listing \ref{code:rs-main} it is shown how a \emph{temperature} resource is registered in the framework.

\lstinputlisting[language=C, caption={Resource handler registration on device.}, label=code:rs-main]{content/code/remote_sensors/device/main.c}

This temperature resource will be exposed at \emph{/temperature/} and the handler \emph{Temperature\_get\_handler} \ref{code:rs-resource-handler} is registered for the \emph{GET} request and when the resource is called (\emph{STREAM}).
The temperature handler will read the values from the \emph{ADC} and set them in an output buffer. 
The Remote Sensor framework will return the contents of the output buffer to the callee.

The constrained device in this example splits the temperature values into 8 bit slots, sets the response's word size to 2 Bytes, and sends them as an \emph{unsigned} natural number.
For a complete device configuration using the Remote Sensors framework refer to \ref{code:rs-full-main}.

\lstinputlisting[language=C, frame=single, numbers=left, caption=Resource Handle, label=code:rs-resource-handler]{content/code/remote_sensors/device/resource.c}

\subsubsection{Gateway}

All the device messages directed to external networks have to go through the Remote Sensors Gateway.
In listing \ref{code:rs-gateway-callback}, it is illustrated how a Remote Sensors callback can be attached to the \emph{message received event} from the device.
In this case, the callback definition only gets called when \emph{Response} messages arrive to the gateway, such the case for \emph{stream} calls. More accurately, the callback handles only responses with data, excluding the \emph{acknowledged} requests' responses.

The \emph{forward\_streams} definition will, in essence, get from the indexed stream requests the request with the same transaction ID than that from the incoming response.
Then the handler will use the URI from the request to publish the response to an internal resource MQTT topic for further processing. In other words, if the stream's URI call was \emph{/temperature/} then the topic will be the same but under KIoT's internal topic, as seen in line \textbf{14-15} of listing \ref{code:rs-gateway-callback}.

In this case, the Gateway application would then publish the event to an internal KIoT MQTT topic from which the Digital Twin will consume, process, and, if applicable, publish the outcome in a public topic for external services to consume. This, however, is not limited to MQTT, \emph{Digital Twins} can choose the prefered communication protocol, e.g., HTTP, websockets, or other.
This will result in independent deployments through the Kubernetes master, where the \emph{scheduler} will allocate stateless applications across the available nodes where resources are best used.

% for the gateway application try to abstract into couple of functions... it became the API :D
\lstinputlisting[language=Python, frame=single, numbers=left, caption=Sensor stream callback, label=code:rs-gateway-callback]{content/code/remote_sensors/gateway/rs_callbacks.py}

For the Remote Sensors Gateway to listen for incoming stream calls or requests, the gateway subscribes to an internal MQTT topic in which it listens for new incoming requests.
Therefore, is up to to the gateway application to handle the required logic to forward the requests to the devices that host the requested resources.

When a service calls for a resource stream, the gateway interprets the incoming request and forwards the request to the device that has the resource, as defined in listing \ref{code:rs-gateway-mqtt-callback}. Analog, when external services wishes to stop the stream, a stream request with a negative state is sent.
In listing \ref{code:rs-full-gateway-callbacks}, it is described how the gateway, in a simplistic manner, forwards the incoming stream request.

Services that require such messages would then subscribe to the desired topic and consume from it.
Once these services consume and process the data, the \emph{Digital Twin} then acts as an isolated event-driven service, which adds flexibility to applications and move heavy computational resources to nodes where resources are available.

This results in a seemless communication between constrained devices and \emph{Digital Twins}.
However, the communication model depends on the applications' use case.
Some applications may benefit more from communicating with different broker endpoints instead of the described model in this thesis, for simplicity, this thesis chose to implement the \emph{internal/external} topic model.

\lstinputlisting[language=Python, caption=Sensor MQTT stream callback, label=code:rs-gateway-mqtt-callback]{content/code/remote_sensors/gateway/mqtt_callback.py}

When deploying IoT gateways to a set of nodes, two possibilities exist. The most simple one is using deployments and deploying into explicitly targeted nodes.
This, however, faces some difficulties. Deployments can allocate multiple instances of the gateway into the same node.
Although this is not a problem for services and simple applications, applications that require block devices will face communication conflicts with the peripheral.
Although this can be prevented by using \emph{nodeSelectors}, \emph{labels}, \emph{node affinity and anti-affinity} rules, this is can be very complicated when scaling a cluster.

The second option is to use a Daemon Set to deploy IoT gateways. Daemon sets ensure that all the nodes that match a criteria run a copy of the pod.
Daemon sets are better suited for gateway applications because this application should be ran at least once in a node.
In the listing \ref{code:rs-gateway-deployment}, a minimal deployment configuration of a deamon set is desribed.

\lstinputlisting[language=yaml, caption=Sensor stream callback, label=code:rs-gateway-deployment]{content/code/remote_sensors/deployment/deployment.yml}

This configuration describes the daemon configuration of the Remote Sensors Gateway.
The Pod specification in the daemon set, in line \textbf{22}, describes through \emph{nodeSelectors} the criteria for the \emph{daemon controller} to place the pods.
In this example, all nodes with the label \emph{remote\_sensors} and value \emph{enabled} and that are \emph{arm} architecture will host a copy of the daemon set.

The biggest benefit from this approach, compared to the Deployment type, is that when a new node joins the cluster and the node qualifies to host a copy of the daemon set, the application will automatically be scheduled.
Moreover, updates to these applications can be now rolled out more gracefully, as the \emph{scheduler} will take care of carrying out the updates on all nodes automatically.

An overview of the process-network interaction can be seen in figure \ref{image:gateway-network-overview}.
In contrast with the highly-coupled LED application, the gateway process uses the network bidirectionally.
Whereas the LED is simply publishing events to a topic, the gateway publishes and subscribes to different topics.

\begin{figure}[h]
  \includegraphics[width=0.42\textwidth]{content/images/04_implementation/gateway-network.png}
  \centering
  \caption{Network overview with different processes.}
  \label{image:gateway-network-overview}
\end{figure}

\subsubsection{Digital Twin}

With this approach, \emph{Digital Twins} become more of a service application and empowers edge devices to be closer to other services.
\emph{Digital twins} in this case can be allocated in any node that fulfills the criteria, it can be allocated remotely somewhere in the cloud or within the nodes.
In the following example it will be illustrated how a digital twin can aggregate and processes the values of a thermometer deployed in a remote constrained device, described in listings \ref{code:rs-main} and \ref{code:rs-resource-handler}.

In listing \ref{code:digi-twin-mqtt-callback}, the callbacks of the MQTT communication are described. In this minimal example, the digital twin calls the stream of the temperature resource by sending a \emph{Stream Request} to the streams topic.
Once the gateway and the remote sensor have started the streaming of the resource, the resource values will be sent as an event in the internal topic of the temperature resource, which the digital twin will listen to by subscribing to the internal temperature topic.
In the rather simplistic \emph{listen\_for\_temperature} method, the digital twin modifies the payload as needed and, consequently, adds them to a cache and a queue.
This allows the data to be persisted and further analysed by other services.

\lstinputlisting[language=Python, caption=Digital Twin MQTT callback, label=code:digi-twin-mqtt-callback]{content/code/digital-twin/mqtt-callbacks.py}

In the other side of the twin is the exposed API.
Digital twins can then expose simple APIs so users or other services can communicate with it in a more traditional way with network protocol that are common in the cloud.

For example, in listing \ref{code:digital-twin-ws} it is described how and endpoint from the \emph{Digital Twin} can expose the cached or queued values through websockets, enabling real time data feeds and falling back to cached values when the queue is empty.
In this case, the Temperature digital twin uses the MQTT topic to receive sensor messages and uses an in-memory datastore, redis, to cache and enqueue those values.
In other words, the nature of volatile \emph{Digital Twins} requires that any state should be persisted in a more stable service or node.
As this twin requires communication from external services, the deployment of this application should be then include a \emph{service} definition.
This will enable other services to communicate with this service through the bind \emph{nodePort}.

\lstinputlisting[language=Python, caption=Temperature endpoint through websockets, label=code:digital-twin-ws]{content/code/digital-twin/wsocket.py}

Digital twins can then be deployed as normal applications across the cluster of nodes, in other words digital twins's \emph{nodeSelectors} can become more loose than other applications.
This will allow the \emph{scheduler} to choose the best fit node automatically for us.
And due to the stateless design of digital twins, these can be deployed anywhere, for the deployment configuration refer to \ref{code:digi-twin-deployment}.

In listing \ref{code:digi-twin-service}, the service specification of the temperature's Digital Twin is described.
This specification allows the application to be exposed to other services by the kubernetes DNS name convention from within the cluster and mapping the container port to the cluster IP's HTTP port.
Furthermore, in listing \ref{code:digi-twin-ingress}, the ingress from an external port is described.
In this configuration, the ingress describes how the request should be like for the load balancer to forward the requests to the matched service.
In the ingress specification the \emph{PathPrefixStrip} rule is used to match requests having the \emph{/temperature-service} in the path.

\lstinputlisting[language=yaml, caption={Digital Twin Service specification}, label={code:digi-twin-service}]{content/code/digital-twin/deployment/service.yml}

\lstinputlisting[language=yaml, caption={Digital Twin Ingress Specification}, label={code:digi-twin-ingress}]{content/code/digital-twin/deployment/ingress.yml}


\begin{figure}[h]
  \centering
  \begin{minipage}{0.27\textwidth}
    \centering
    \includegraphics[width=0.9\textwidth]{content/images/04_implementation/digital-twin-network.png}
    \caption{Network overview.}
    \label{image:gateway-network-overview}
  \end{minipage}\hfill
  \begin{minipage}{0.7\textwidth}
    Overall, the figure \ref{image:gateway-network-overview} illustrates the network traffic flowing through the Digital Twin. \\
    The outbound connections would be the MQTT connection to the broker and the connection to the redis datastore.

    While the incoming traffic, specified through the \emph{nodePort} configuration of the load balancer, is redirected from the \emph{traefik-ingress} service to the temperature service.
      \end{minipage}\hfill
\end{figure}

\section{The big picture}

% This thesis does not have to change anything regarding Kubernetes but it simply uses it by configurting accordinglt.
The KIoT system consists of technologies from different industries, specially from cloud computing.
The system aims to reduce technology debt in distributed IoT systems by using Docker containers to isolate runtime configurations.
And by following the UNIX process model, Kubernetes acts as the system's process supervisor.

In figure \ref{image:the-big-one}, a rough sketch of the technologies used and subnetworks are illustrated.
Commodity hardware, Raspberry Pis, were used in the IoT platform which consists of three edge devices.
In this case, the hardware used were Raspberry Pis revision 3 running Linux (4.14.68-v7+) with Docker (v18+), Kubernetes (v1.11), and the OpenVPN client.
For implementing the highly coupled processes a LED was installed directly using the GPIO pins of a Raspberry Pi.

\begin{figure}[h]
  \includegraphics[width=0.6\textwidth]{content/images/04_implementation/big-picture.pdf}
  \centering
  \caption{The KIoT system in a nutshell.}
  \label{image:the-big-one}
\end{figure}

For the Remote Sensors Framework, an XBee radio was connected through one of the USB ports and mounted to the containers.
For the Remote Sensors, an Arduino UNO with a temperature sensor and the XBee radio were attached.
The IEEE 802.15.4 network protocol was used for the communication within the Remote Sensor applications.

The \emph{Git} provider for this implementation was Bitbucket Cloud and all the code samples can be found under the \emph{kiotsystem} team.
The Container image registry used in this implementation was provided by \emph{Docker.io}.
All images can be found, as well, under the \emph{kiotsystem} organization.

The cloud components used along side the IoT platform were hosted in different cloud providers and in different availability zones.
For the automation tools, a mixed of cloud providers was used due to architecture support.
For the automations tools to execute CI pipelines in \emph{ARM} architectures, the cloud provider \emph{Scaleway} in \emph{par1} (Paris-1) availability zone was used.
For the \emph{amd64} architecture instances in the availability zone \emph{fra-1} (Frankfurt-1), DigitalOcean was used.
The cloud services, such as the MQTT broker, redis datastore, and OpenVPN were deployed in different instances in the \emph{fra-1} availability zone.
All of which, use the Internet as their backbone network.

The implementation example described above illustrates the different use cases that an IoT platform can achieve under the KIoT system.
From all the opportunities that Kubernetes provides to an IoT platform, the best solution it offers is the possibility to offer an IoT platform with \emph{Best Effort Consistency}.
And by using technologies that are portable across system architectures, development and artifact delivery becomes more uniform and transparent.

Development of applications can be isolated by using Docker containers.
Delivery, in contrast, is the only configuration that has to be properly set in order to be able to deliver IoT applications with Kubernetes.
As illustrated previously with the \emph{deployment} and \emph{daemon set} configurations, the KIoT system uses native Kubernetes configuration and with minimal configuration alteration.
For reference on a minimal node configuration refer to \ref{code:ansible-playbook} and \ref{code:functions}.
