\chapter{Design}

In this chapter the design of the KIoT (/\textipa{ka\;I'(j)oUti}/) system is presented and explained. The chapter goes into the two main components of the system that together assemble a complete Kubernetes backed \acrshort{IoT} platform. And defining the functional and non-functional requirements of the KIoT system.

\section{Continuous Practices}

Continuous practices are crucial for an agile development workflow. Therefore, a system that supports them is imperative. Overall, as it is illustrated in the figure \ref{diagram:artifact_flow_overview}, when a developer commits his changes to a \acrshort{VCS}, the KIoT system triggers a predefined pipeline in the Automation Tools, e.g., in a \acrshort{CI} server, in which the version of the code is submitted to a series of stages that build and test the artifact. If the version of the code defines a deployment, then the system should execute a deployment once the previous stages succeeds.

For the KIoT system to be able to execute such pipelines, the system is composed, as illustrated in figure \ref{diagram:artifact_flow_overview}, by the following major components, \emph{\acrshort{VCS}}, \emph{Automation Tools}, \emph{Image Registry} and an \emph{\acrshort{IoT} platform}.

\begin{figure}[h]
  \includegraphics[width=0.6\textwidth]{content/images/03_design/system_design_overview.pdf}
  \centering
  \caption{Interaction between the different components of the system for a \acrshort{CDE}.}
  \label{diagram:artifact_flow_overview}
\end{figure}

In order for the KIoT System to implement Continuous practices, a certain behaviour has to be fulfilled. The KIoT system requires the application code to be tracked by a \acrshort{VCS}, e.g., Git. Different branching models exists and each one of them aim to solve different problems in different ways. However, one of the most popular branching models is based on \emph{Release Candidate} branches. When a feature branch ends development this goes to a \emph{preview}/\emph{develop} branch. Once the feature has been tested and is approved by the quality checks, this feature joins a Release Candidate branch which usually consists of a pool of features and bugfixes to release. Once the Release Candidate branch fulfills the quality checks, this branch is then released by merging it to the main branch, \emph{master}, or by creating a \emph{release} tag.

Automation Tools, in this case our CI server, executes the pipelines. This pipelines consist of different stages that execute a set of reproducible and consistent steps, which are configured in each application repository. The CI server will then queue the stages in wait for an agent to be available to execute such steps.

\begin{figure}[h]
  \includegraphics[width=0.7\textwidth]{content/images/03_design/system_design.pdf}
  \centering
  \caption{Components and subcomponents of the \acrlong{CI}.}
  \label{diagram:artifact_flow}
\end{figure}

Although, the pipeline steps diverge according to each artifact's use case, one of the most common set-ups is as illustrated in figure \ref{diagram:artifact_flow}. These steps consist on running low-level tests, e.g., linting or unit tests. Consequently, the following stages are assembling or building the artifact's container image and pushing the image to a registry. Once the artifact's container has been built and passed the tests, the artifact's container is submitted to a more comprehensive test suite, e.g., Acceptance tests.

After the artifact has satisfied the tests, a manual or automatic deployment to the KIoT's \acrshort{IoT} platform can be triggered, e.g., trigger a deployment pipeline through the Automation Tools. Once the \acrshort{IoT} platform has scheduled the respective pods and is ready to rollout the update, the assigned node will pull the container image from the registry and, consequently, execute or update the application.

Therefore, we can conclude that when a developer uses the system, the following requirements are present:

\begin{enumerate}
  \item On \acrshort{VCS} state change, the Automation Tools trigger the predefined pipeline.
  \item The artifact is submitted to a series of tests, providing feedback to the developer.
  \item The version of the code is assembled and built into a container, providing feedback to the developer.
  \item Artifact deployments are done in an automatic and reprodusable way, providing feedback to the developer.
  \item Feedback can be consulted when needed by the developer.
\end{enumerate}

% ------------------------ IoT Platform ------------------------ %

\section{\acrshort{IoT} Platform}

Once the Automation Tools contact the IoT platform for a deployment, the artifact's container image has to be already published in the Image Registry.
As the deployment is requested, the Kubernetes backed \acrshort{IoT} platform schedules the deployment accross the targeted nodes.

The KIoT's \acrshort{IoT} platform component is composed, as illustrated in figure \ref{diagram:iot_platform}, by a master, nodes and attached/connected devices.
The attached or connected devices communicate over a contrained network or through the node's board ports.
The largest and most critical component of the KIoT system is the network.
KIoT's network takes advantage of an already existing network, the Internet.
By using the Internet as the network's backbone, it allows distributed nodes to easily communicate with cloud components.

\begin{figure}[h]
  \includegraphics[width=0.5\textwidth]{content/images/03_design/iot_platform.pdf}
  \centering
  \caption{Components of an \acrshort{IoT} platform backed by Kubernetes.}
  \label{diagram:iot_platform}
\end{figure}

Fog Computing establishes a new set of requirements for IoT platforms.
While allowing edge devices to be more independent, artifacts deployed to the devices have to be maintained.
Furthermore, different requirements arise from \emph{Digital Twins} and \acrshort{IoT} gateway implementations.

In the terms of \emph{Digital Twins}, devices are represented in two dimensions, in the physical and in the digital world. While keeping a digital representation of a device is easier than guaranteeing the availability of the device or sensor, the architecture that the digital representation requires for offering a highly available service is critical for cloud components to consume from this service.

Overall, the KIoT's \acrshort{IoT} platform component is based on the gateway pattern. As illustrated in the figure \ref{diagram:iot_platform_management}, this component is a distributed subsystem using a subnetwork configured by Kubernetes' CNI plugin. Edge Devices or Gateways can connect to such network and become members of the IoT platform network infrastructure.

Nodes in the KIoT system can act as an edge device or a gateway device.
A device that acts as an edge device is capable of producing and consuming data directly from sensors or the platform itself, e.g., an edge device that produces and consumes data from physically-attached sensors.
In the other hand, nodes acting as \acrshort{IoT} gateway can communicate through attached devices and expose the device data through \emph{Digital Twins}.
Management of artifacts and devices can be done by direct communication with the master node from the platform, this interaction is illustrated in figure \ref{diagram:iot_platform_management}.

\begin{figure}[h]
  \includegraphics[width=0.45\textwidth]{content/images/03_design/system_design_user_interaction.pdf}
  \centering
  \caption{Management of the platform.}
  \label{diagram:iot_platform_management}
\end{figure}

Communication over a distributed network, specially in the \acrshort{IoT} industry, is highly unreliable.
According to the CAP theorem \cite{cap_theorem}, a web service must trade off consistency, availability, and partition tolerance.
Naturally, this concept applies as well for a distributed network of sensors.
In order to overcome CAP's negative implications, solutions like Best-Effort Availability or Best-Effort Consistency are implemented.
While a balance between availability and consistency is desired, network unreliability in distributed \acrshort{IoT} platforms is often present.
Therefore guaranteeing a consistent system is practically impossible.
KIoT does not neglect the presence of a partitioned network, it embraces it.
The KIoT's IoT platform component, expects nodes to eventually fail.
On node failure, KIoT attempts to reschedule the artifacts into a similar node that fulfills the artifacts' criteria.
Thus, Best Effort Consistency is achieved by the implementation of \emph{Digital Twins}.
These relaxes the consistency of the devices and guarantees always a valid response.
If the node hosting the \emph{Digital Twin} goes offline, the platform will reschedule it in a similar node.
This is complemented by having an Event-Driven communication, as it aids the platform to preserving the anonymity of the services and, therefore, allowing the presence of less coupled applications over a distributed network while allowing high volume and velocity of data flow.

\begin{figure}[h]
  \includegraphics[width=0.65\textwidth]{content/images/03_design/event_based_platform.pdf}
  \centering
  \caption{Event-Driven architecture in the \acrshort{IoT} platform.}
  \label{diagram:event_based_platform}
\end{figure}

In the KIoT system, devices that communicate over a \emph{Network Translator} are exposed to the proper resource topic on the Event-Driven backend, as illustrated in figure \ref{diagram:event_based_platform}.
When sensor data is produced this will be pushed to the message broker, forwarding the message to all subscribers.
Hence, a \emph{Digital Twin} instance will consume the produced data and act as the device frontend, e.g., an API for the physical device, allowing remote access to the digital representation.
The reverse implies the Network Translator (Gateway) to consume data from the message broker and forwarding to the device, so then the device can react upon it.
All these components live inside the distributed nodes of the proposed \acrshort{IoT} platform.

Therefore, to fulfill the requirements of an \acrshort{IoT} platform that provides a \emph{Digital Twin} and supports Edge Devices, the following functionality is required:

\begin{enumerate}
  \item Consistent communication across artifacts, nodes, and cloud components.
  \item Edge devices can join the platform securely.
  \item If a node goes offline, the artifacts gets reschedule to a similar node, if available.
  \item \acrshort{IoT} gateways translate the network protocols and react upon it.
  \item Digital Twins are disposable and survive independently of the physical device.
  \item Digital Twins are identifiable as a service in the platform.
\end{enumerate}

\section{The KIoT System}

The KIoT system in a nutshell is the integration of Continuous Practices and an \acrshort{IoT} platform, aiming to empower developers to react quicker to business changes and deliver products more frequently while preserving centralized node and artifact management.
The KIoT System is composed by several components, each one empowering different aspects of the product development cycle, the complete system can be illustrated in figure \ref{diagram:kiot}.

\begin{figure}[h]
  \includegraphics[width=1\textwidth]{content/images/03_design/kiot.pdf}
  \centering
  \caption{The KIoT in a nutshell.}
  \label{diagram:kiot}
\end{figure}
