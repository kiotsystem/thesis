\chapter{Introduction}

\newacronym{IoT}{IoT}{Internet of Things}

In the world of process automation, the \acrfull{IoT} plays a critical role.
Whether it is for a rather simplistic goal, much like measuring the temperature of a room, to a more complex one, e.g., network protocol translations between constrained devices.
The possibilities that these imply for industries that use or implement IoT products are endless.

As today's devices get further developed and improved, these devices become more powerful and efficient, making them more reliable, safer, and faster to deploy.
As reported by Gartner, by 2020 approximately 20 billion IoT devices will be installed \cite{website:gartner_installed_devices}.
According to the same report, in 2017 the targeted applications to vertical industries, e.g., manufacturing field devices and real-time location devices for healthcare, will drive the use of connected devices in the businesses.
And by 2018 the category that leads the use of the connected devices will be the cross-industry devices, e.g., devices targeted to smart buildings, which will benefit from the drop in cost, leading as the connectivity is driven into a higher volume.

With devices getting more efficient and powerful, devices are now able to analyze data closer to the device that collected it, minimizing the latency and preserving network bandwidth \cite{cisco_fog_computing}.
Allowing private data, e.g., personal data, to be locally processed and analyzed before being sent off to the cloud.
Being able to analyze the data closer to the device that produced it is known as \emph{Fog Computing} \cite{cisco_fog_computing}.

\newacronym{CI}{CI}{Continuous Integration}
\newacronym{CD}{CD}{Continuous Delivery}

However, the numerous amount of devices that are being deployed every day, forces software and hardware development to become faster in order to fulfill business' requirements.
In such quick environments, development and deployment of artifacts are usually the slowest and most sensitive part of the product's development cycle.
As Sam Guckenheimer states when defining \acrfull{CI}, developers usually work in isolation and once they have finished, such work has to be merged into the team's codebase.
Nonetheless, developers find themselves waiting long periods of time to test and merge their code, which could then lead to conflicts and hard-to-fix bugs, diverging code strategies and duplicated efforts \cite{website:microsoft_continuous_integration}.
This process of automating the build and testing of code every time code gets merged into the designated version control is known as \acrshort{CI} and has emerged as one of the best practices for developers \cite{website:microsoft_continuous_integration}.
On the other hand, improving the development cycle of a product has to be complemented by an autonomous deployment workflow.
Hence, the process of building, testing, configuring and deploying artifacts is known as \acrfull{CD}, as defined by Sam Guckenheimer.
The main goal of CD is to have the shortest safe path to deploy the freshest artifact into an environment.
Therefore, by having \acrshort{CD} the workflow of deploying artifacts will minimize the time to deploy and potentially mitigating or remediating production incidents \cite{website:microsoft_continuous_delivery}.

However, as business requirements evolve, the development and delivery of artifacts has to match the business' pace.
Furthermore, on the surge of connected devices, management and maintenance of such devices, their artifacts, and their data has become more critical than before.

\section{Motivation}

\newacronym{SDLC}{SDLC}{Software Development Life Cycle}

As Gartner identified, it can sometimes be hard for business to aquire such IoT-related skills from in-house talents \cite{website:gartner_hype_cycle} and as technologies in the IoT evolve we see more powerful devices capable of leveraging the lack of IoT-related skills.
However, development and deployment of IoT solutions is a not a complete stranger.
By identifying the development process of an IoT product, we can identify a common cycle when compared to other IT industries, \emph{Requirement Gathering}, \emph{Design}, \emph{Development}, \emph{Test}, \emph{Production and Support} \cite{website:gartner_iot_product_launch}.
The same cycle that is commonly found in any other \acrfull{SDLC} \cite{uni_due_softwaretech}.

From a Software Development, as Hardware Development is outside this thesis' scope, the \acrshort{SDLC} is constant or slighlty changed.
Therefore, a comparison between the \acrshort{SDLC} of a product targeted for the cloud can also be applied to the development of a product in the \acrshort{IoT} scope.

One of the software development methodologies that has gathered attention, is the Agile methodology alongside the Scrum framework.
These two have proven to be beneficial on the development and deployment of artifacts, as nowadays organizations experience a rather high-pace velocity of business change making markets to appear or dissolve in a matter of weeks or months \cite{report:transforming_application_delivery}.
These methodologies allow organizations to react in time to business changes and to deliver the best fitting product.

Two of the twelve principles that the Agile Manifesto defines is that it should be able to satisfy the customer through the early and continuous delivery of valuable software while reducing the delivery frequency with a preference to a shorter timescale \cite{report:agile_manifesto}.
Therefore, making CI and CD an essential part of the product's development and delivery process.

Nowadays, the technology stack a developer can have is vast, giving the opportunity to adapt accordingly to the different requirements and enabling faster software development.
Nevertheless, all these different technologies face problems during the implementation and delivery, e.g., overlapping runtime configuration or artifact dependencies.
And as artifacts get developed based on specific environments, isolated runtime configurations are required.
Thus, environments with technologies like containers aid in runtime configuration isolation.
Allowing teams to have the possibility to develop and deploy artifacts that are programming language agnostic and, most important, runtime configuration agnostic.

As today's markets change abruptly and the \acrshort{IoT} industry matures, teams must be able to conceive, produce, and deliver artifacts at an unprecedented pace.
This affects the nature of the \acrshort{IoT} artifacts to become not only highly volatile but also highly distributed.
Thus having an orchestrator that manages deployments of such artifacts in the targeted devices, while being an autonomous and self-healing \cite{documentation:kubernetes} platform is a crucial element for a fast and reliable artifact delivery.

\section{Tasks}

In the nature of things, IoT systems are often deployed in remote locations, resulting in distributed systems.
As more and more devices are deployed, teams have to be able to more reactive.
The goal of this thesis is to investigate a system that allows faster and more flexible development while increasing the reliability of artifact deployment into IoT systems.

This thesis focuses on using existing technologies that aim to solve problems from distributed systems.
Cloud computing has solved runtime configuration isolation with the use of containers, and management of distributed systems through container orchestrators.
As cloud computing and IoT systems evolve and the gap between them is reduced, technologies can now be ported.

In this manner, the use of containers, allows the development process to become faster, more flexible, and isolate artifacts' runtime configurations.
However, high volume container deployments are hard to manage and maintain.
Therefore, the use of container orchestrators enhances the delivery process by making it more frequent and reliable.

\newacronym{K8s}{K8s}{Kubernetes}

In the last couple of years, a system that has gained attraction is \acrfull{K8s}.
K8s is an open-sourced system for automating deployment, scaling, and management of containerized applications, open-sourced by Google.
By grouping containers that make up an application into logical units, K8s is capable of offering an easier application management.

This thesis investigates how K8s, containers, and continuous practices can be used in an IoT system, in order to ease development and deployment of distributed IoT applications. The system will then take advantage of K8s' features, some of them being device management, monitoring, self-healing, and automated deployments. As the usage of continuous practices, such as \acrshort{CI} and \acrshort{CD}, enhances the product development cycle and is an essential part of the agile methodologies, the continuous practices are considered and an adapted development process is proposed.

This thesis introduces the KIoT (/\textipa{ka\;I'(j)oUti}/, pronounced as \emph{coyote}) System. This system aims to solve key problems from IoT Systems and development processes by software development lifecycle automation, adapting accordingly to continuous practices, isolation of artifact's configurations, and a centralized and comprehensive centralized device and artifact management tool.

In order to achieve the system model described above, the following tasks have to be fulfilled.

\begin{enumerate}
  % This is done in chapter 2.1
  \item \textbf{KIoT-1} Compare the IoT software development lifecycle with a K8s software development lifecycle.
  % This is done in chapter 2.1.3 and in 2.3
  \item \textbf{KIoT-2} The system enables automatic and continuous delivery of artifacts.
  % this is done in 2.2
  \item \textbf{KIoT-3} Isolation of IoT applications' runtime configurations.
  % this is acieved by weavenet
  \item \textbf{KIoT-4} IoT applications are unaware of the network topology.
  % this is done alongside with 2.3
  \item \textbf{KIoT-5} Centralized management of IoT devices and artifacts.
\end{enumerate}


\section{Work's Structure}

This work is built in different chapters, each built upon the previous one.
In chapter \textbf{2}, fundamentals of the KIoT system are discussed, such as containers, Kubernetes, Software Development Life Cycles, among others.
The fundamentals aim to set a common foundation of knowledge for the upcoming chapter.
In chapter \textbf{3}, the design of the KIoT System is presented.
This chapter goes into the system's architecture and the technologies used.
Following up from design, chapter \textbf{4} discusses the implementation of the KIoT system and how IoT applications should be architectured for the KIoT system.
In chapter \textbf{5}, the system design and implementation are evaluated.
And in chapter \textbf{6}, the investigation is concluded and a brief outlook is given.

