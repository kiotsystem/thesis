import json
import remote_sensors

from remote_sensors import Request, Response, app
from remote_sensors.xbee import SensorRequest
from remote_sensors.log import logger

import mqtt
import settings
import xbee as device

@remote_sensors.bind_to_device()
@remote_sensors.request()
def access_log(request: Request, *args, **kwargs):
    logger.info(f'Request: {request.method} - {request.uri}')


@remote_sensors.bind_to_device()
@remote_sensors.request(method='REGISTRATION', pass_message=True)
def handle_registration(request: Request, sensor_message: SensorRequest,
                        *args, **kwargs):
    """Handles the registration request."""
    response = Response(status='ACK',
                        transaction=request.transaction)
    # do something about it :D
    payload = {
        'device': sensor_message.as_dict(),
        'request': request.as_dict(),
        'response': response.as_dict(),
        'completed': False,
    }
    topic = f'/{settings.MQTT_INTERNAL_NAMESPACE}/registrations'
    mqtt.client.publish(topic, json.dumps(payload))

    return response


@remote_sensors.bind_to_device()
@remote_sensors.request(method='END_REGISTRATION', pass_message=True)
def handle_reg_end(request: Request, sensor_message: SensorRequest,
                   *args, **kwargs):
    """Handles the end of registration."""
    response = Response(status='ACK',
                        transaction=request.transaction)
    # do something about it :D
    payload = {
        'device': sensor_message.as_dict(),
        'request': request.as_dict(),
        'response': response.as_dict(),
        'completed': True,
    }
    topic = f'/{settings.MQTT_INTERNAL_NAMESPACE}/registrations'
    mqtt.client.publish(topic, json.dumps(payload))

    return response


@remote_sensors.bind_to_device()
@remote_sensors.response(status='SEND') # we are not interested in the ACK
def forward_streams(response: Response, streams: dict, *args, **kwargs):
    """Forward the stream responses."""
    payload = response.as_dict()

    request: Request = streams.get(response.transaction)
    topic: str = mqtt.get_internal_topic_from(request)

    mqtt.client.publish(topic, json.dumps(payload))


@mqtt.subscribe(f'/{settings.MQTT_INTERNAL_NAMESPACE}/streams')
@mqtt.json_message()
@mqtt.stream_request()
def listen_for_stream_requests(stream_request: mqtt.StreamRequest,
                               *args, **kwargs):
    """Handles a request to start or stop a stream."""
    request = stream_request.as_remote_sensors_request()

    # check if we have a stream already
    transaction = app.search_stream_request(request)

    device.send_async(stream_request.addresses, request)

    if not transaction:
        app.streams[request.transaction] = request # add it to our streams
    if transaction and not stream_request.state:
        del app.streams[request.transaction]
