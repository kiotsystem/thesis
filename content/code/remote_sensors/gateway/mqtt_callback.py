import mqtt
import remote_sensors
import xbee as device
from mqtt import StreamRequest

@mqtt.subscribe(f'/{settings.MQTT_INTERNAL_NAMESPACE}/streams')
@mqtt.json_message()
@mqtt.stream_request()
def handle_stream_requests(stream_request: StreamRequest,
                           *args, **kwargs):
    request = stream_request.as_remote_sensors_request()

    # check if we have a stream already
    transaction = remote_sensors.search_stream_request(request)
    device.send_async(stream_request.addresses, request)

    if not transaction:
        remote_sensors.app.streams[request.transaction] = request
    if transaction and not stream_request.state:
        del remote_sensors.app.streams[request.transaction]
