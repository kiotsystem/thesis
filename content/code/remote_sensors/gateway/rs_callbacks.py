import json
import remote_sensors
import mqtt
from remote_sensors import Request, Response

@remote_sensors.bind_to_device()
@remote_sensors.response(status='SEND')
def forward_streams(response: Response, streams: dict,
                    *args, **kwargs):
    """Forward the stream responses."""
    payload = response.as_dict()
    request: Request = streams.get(response.transaction)

    topic: str = mqtt.get_internal_topic_from(request)
    mqtt.client.publish(topic, json.dumps(payload))
