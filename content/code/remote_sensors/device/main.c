#include <remote-sensor/api.h>
#include "src/handles/index.h"
/* Temperature resource registration */
void
register_temperature(void)
{
    Resource* resource = ResourceManager.create("read",
        IS_RESETTABLE, SINGLE_BUFFER, RESOURCE_MANAGER_DEFAULT);
    PeripheralReadHandle read_handle = &Temperature_get_handler;
    ResourceManager.attach_read_handle(read_handle, resource);
    PeripheralCallHandle call_handle = &Temperature_get_handler;
    ResourceManager.attach_call_handle(call_handle, resource);
    // register under the uri: '/temperature/'
    URIComponent* uri = ResourceManager.create_identifier("temperature");
    ResourceManager.register_resource(resource, uri);
    ResourceManager.register_component(uri,
        ResourceManager.get_root_component());
}
