#include <remote-sensor/api.h>
#include "handles.h"

/* Remote Sensors Handler API */
void
Temperature_get_handler(StreamBuffers* buffers, Request* request,
    Parameter* parameters[], Response* response) {
    uint16_t reading = ADC_read(0);
    // ... conversion
    BufferManager.push((reading & 0xFF), buffers->output);
    BufferManager.push((reading >> 8), buffers->output);
    response->word_size = 0x2;  // 2 Byte
    response->content_type = UNSIGNED_TYPE;
}
