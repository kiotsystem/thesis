/* Definitions */
#define BAUD_RATE 9600
/* library imports */
#include <avr/io.h>
#include <util/delay.h>
#include <stdio.h>
#include <stdint.h>

/* imports */
#include <remote-sensor/api.h>
#include <vendor/IOPinController.h>
#include <vendor/Dipswitch.h>
#include "src/sensors/adc.h"
#include "src/handles/index.h"

/* Function declarations */
void
system_init();

int
main(void)
{
    /** Initializes the system */
    system_init();
    uint8_t address[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    size_t address_size = 8;  // bytes
    const uint8_t NUMBER_OF_ATTEMPTS = 0x80;
    /* register the resources in the gateway */
    RegistrationController.register_to(address,
        address_size,
        NUMBER_OF_ATTEMPTS);

    while (1)
        CommunicationController.listen(address, address_size);
}

void
system_init(void)
{
    // initializes the ADC reader
    ADC_init();
    // init resource manager
    uint8_t buffer_size = 0x06;
    uint8_t params_size = 0x05;
    size_t stream_request_list_size = 5;
    RequestAcceptType accept_requests = ACCEPT_STREAM;

    CommunicationController.init(accept_requests,
        stream_request_list_size,
        params_size,
        buffer_size);

    /* Temperature resource registration */
    Resource* resource = ResourceManager.create("read", IS_RESETTABLE,
            SINGLE_BUFFER, RESOURCE_MANAGER_DEFAULT);
    PeripheralReadHandle read_handle = &Temperature_get_handler;
    ResourceManager.attach_read_handle(read_handle, resource);
    PeripheralCallHandle call_handle = &Temperature_get_handler;
    ResourceManager.attach_call_handle(call_handle, resource);

    // register the temperature resource under the uri: '/temperature/'
    URIComponent* uri = ResourceManager.create_identifier("temperature");
    ResourceManager.register_resource(resource, uri);

    ResourceManager.register_component(uri,
        ResourceManager.get_root_component());
}
