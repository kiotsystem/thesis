#!/bin/bash

set_defaults() {
  # check for the PODS IP CIDR
  if [ -z "${PODS_IP_CIDR}" ]; then
    export PODS_IP_CIDR='10.244.0.0/16'
  fi
}
###
# Initializes the kubeadm, will make the initial token live for ever,
# token-ttl=0
# :param 1: Pod network Cidr, e.g. '10.244.0.0/16'
###
init_kubeadm() {
  local pod_network_cidr=${1:?'Please give an addres for the pods cidr'}
  echo '## Will initialize kubeadm ##'
  kubeadm init --pod-network-cidr ${pod_network_cidr} --token-ttl=0
  echo '## Finished kubeadm init ##'
  echo '## Will copy kubeadm configuration ##'
  # copy the configuration to user's home dir
  mkdir -p ${HOME}/.kube
  cp -f /etc/kubernetes/admin.conf ${HOME}/.kube/config
  chown $(id -u):$(id -g) ${HOME}/.kube/config
  echo '## Kubeadm configuration in place ##'
}
###
# Prints out a fresh token for nodes to join the cluster
# :param 1: token
###
get_token() {
  local token=${1:?'Please provide a token.'}
  local ttl=0
  kubeadm token create ${token} --print-join-command --ttl=${ttl}
}
###
# Sets the weave network as plugin
#
# :param 1: kubectl version base64 encoded.
# :param 2: pod network cidr
###
set_weave_network() {
  local kubectl_version=${1:?'No kubectl version was given.'}
  local pod_network_cidr=${2:?'No network Cidr was given.'}
  # deactivate fastdp, https://github.com/weaveworks/weave/issues/3314
  opts="env.WEAVE_NO_FASTDP=1&env.IPALLOC_RANGE=${pod_network_cidr}"
  local endpoint="https://cloud.weave.works/k8s/net"
  local weave_endpoint="${endpoint}?k8s-version=${kubectl_version}&${opts}"

  echo "URL constructed: ${weave_endpoint}"
  kubectl apply -f ${weave_endpoint}
}
