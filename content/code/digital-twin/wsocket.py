import time
from bottle import request
from geventwebsocket import WebSocketError
from app import app
from cache import cache as redis

@app.get('/temperature')
def temperature_consumer():
    wsocket = request.environ.get('wsgi.websocket')
    while True:
        try:
            temp = redis.lpop('temperature:queue')
            if not temp:
                temp = redis.get('temperature')
            wsocket.send(temp.decode('utf8'))
            time.sleep(1)  # avoid saturating the connection
        except WebSocketError as e:
            break
