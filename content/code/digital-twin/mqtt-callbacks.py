import json
import mqtt
import settings
from cache import cache as redis
from models import (Temperature, StreamRequest)

@mqtt.subscribe(f'/{settings.MQTT_INTERNAL_NAMESPACE}/temperature')
@mqtt.json_message()
def listen_for_temperature(payload: dict, *args, **kwargs):
    try:
        temperature = Temperature(**payload)
    except Exception as e:
        return  # invalid value

    temp = temperature.as_dict()
    temp['unit'] = 'Celcius'  # add unit and other transformations...
    temp = json.dumps(temp)

    redis.set('temperature', temp)  # set the latest value
    redis.rpush('temperature:queue', temp)

@mqtt.on_connect()
def request_streams(client, *args, **kwargs):
    streams_topic = f'/{settings.MQTT_INTERNAL_NAMESPACE}/streams'
    stream_request = StreamRequest(uri='/temperature', state=True,
                                   addresses=(None, None))
    client.publish(streams_topic,
                   json.dumps(stream_request.as_dict()))
