/* Generic LED module */
mod led;
fn handle(mut stream: UnixStream) {
    let mut ledRepr = String::new();
    stream.read_to_string(&mut ledRepr).unwrap();
    let state = led::parse(&mut ledRepr).unwrap();
    led::apply(&state).unwrap();
    let response = led::dump(&state).unwrap();
    let payload = response.into_bytes();
    stream.write(&payload).unwrap();
    stream.shutdown(Shutdown::Both).unwrap();
}
fn main() {
    let socket_path = env::var("SOCKET_PATH").unwrap();
    let socket = Path::new(&socket_path);
    let listener = UnixListener::bind(&socket).unwrap();
    for stream in listener.incoming() {
        match stream {
            Ok(stream) => { thread::spawn(|| handle(stream)); }
            Err(err) => { break; }
        }
    }
}
