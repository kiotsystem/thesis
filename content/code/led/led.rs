/* LED module */
/* extern crates */
extern crate rppal;
extern crate serde;
extern crate serde_json;
/* external imports */
use rppal::gpio::{Gpio, Mode, Level};

#[derive(Serialize, Deserialize)]
pub struct LEDState {
    pub pin: u8,
    pub value: bool,
}

pub fn parse(data: &String) 
    -> Result<LEDState, self::serde_json::Error> {
    let state: LEDState = serde_json::from_str(data)?;
    Ok(state)
}

pub fn dump(state: &LEDState)
    -> Result<String, self::serde_json::Error> {
    let data = serde_json::to_string(state)?;
    Ok(data)
}

pub fn apply(state: &LEDState)
    -> Result<(), rppal::gpio::Error> {
    let mut gpio = Gpio::new()?;
    gpio.set_clear_on_drop(false);
    gpio.set_mode(state.pin, Mode::Output);
    let level = match state.value {
        true => Level::High,
        false => Level::Low,
    };
    gpio.write(state.pin, level);
    Ok(())
}
