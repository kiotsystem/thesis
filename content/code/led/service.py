import asyncio
import settings
from led import LED
from mqtt import MQTT

async def blink(led: LED):
    topic = f'{settings.MQTT_RESOURCE}/{led.pin}'
    async with MQTT(settings.MQTT_URL) as client:
        while True:
            led.toggle()
            await client.publish(topic, led.asbytearray())
            await asyncio.sleep(1)

async def main():
    led = LED(pin=settings.LED_PIN, value=True)
    led.bind(settings.SOCKET_PATH)
    led.apply()

    await blink(led)

if __name__ == '__main__':
    run(main())
