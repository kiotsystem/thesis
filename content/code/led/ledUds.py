import json
import socket
from dataclasses import dataclass, asdict

@dataclass
class Stream:
    """Optimistic Stream"""
    address: str

    def __enter__(self):
        stream = socket.socket(socket.AF_UNIX)
        stream.connect(self.address)
        self._stream = stream
        return stream

    def __exit__(self, exc_type, exc_value, traceback):
        self._stream.close()
        return True

class SocketBind:
    """Allows an Object to bind to a socket.
    Communication protocol is agreed before-hand.
    """
    def bind(self, address: str):
        self.address = address

    def _send(self, payload: bytearray):
        """Send the bytearray in the socket."""
        if not self.address:
            return

        received = None
        with Stream(self.address) as stream:
            stream.sendall(payload)
            # we are done talking
            stream.shutdown(socket.SHUT_WR)
            # receive the message
            received = stream.recv(len(payload) + 1)

        return received

@dataclass
class State:
    """Represents the state of the led."""
    pin: int
    value: bool = True

    def asdict(self):
        return asdict(self)

    def asbytearray(self, encoding: str = 'utf-8'):
        string = json.dumps(self.asdict())
        return bytearray(string, encoding)

@dataclass
class LED(State, SocketBind):
    """LED abstraction."""
    def apply(self):
        self._send(self.asbytearray())

    def set_to(self, value: bool):
        self.value = value
        self.apply()
        return self

    def toggle(self):
        self.value = not self.value
        self.apply()
        return self
