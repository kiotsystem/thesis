#!/bin/sh

open_abstract() {
  vim ${PWD}/content/chapters/abstract.tex
}

open_biblio() {
  vim ${PWD}/content/references/references.bib
}
