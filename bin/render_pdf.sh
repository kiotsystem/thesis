#!/bin/bash

#####################
#   Generating PDF
# there's a sequence to generate first the aux files then the bib files,
# after the twice the pdflatex should include all the deps required into the PDF.
# (pdflatex -> bibtex)
#           -> pdflatex -> pdflatex
# https://tex.stackexchange.com/questions/204291/bibtex-latex-compiling
#####################
function do_pdf {
  if [ ! -f "${1}.aux" ]; then
    echo '----- aux files do not exist, generating them... -----'
    # generate aux files
    pdflatex ${1}  # outputs stdout to /dev/null, except stderr

    makeglossaries ${1}

    # create bib
    echo '----- Generating bib files -----'
    bibtex ${1}.aux
  fi

  # create PDF (1)
  pdflatex ${1} > /dev/null
  # create PDF (2)
  echo '----- Will generate final PDF -----'
  pdflatex ${1} > /dev/null
}

echo "----- Will render PDF for ${1} -----"
# do pdf once more
do_pdf ${1}
