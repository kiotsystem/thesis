# Thesis

[![Build Status](https://ci.moreandcoffee.com/api/badges/kiotsystem/thesis/status.svg?branch=master)](https://ci.moreandcoffee.com)

## Usage

```bash
$ docker pull arnulfosolis/latex:latest
$ sudo make build
# to clean
$ sudo make clean
```

## Contact

- Arnulfo Solis
- arnulfojr94@gmail.com

## License

[![Creative Commons Attribution-ShareAlike 4.0 International License](https://i.creativecommons.org/l/by-sa/4.0/80x15.png)](http://creativecommons.org/licenses/by-sa/4.0/)
